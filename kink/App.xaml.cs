﻿#define DEV
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Collections;
using System.Threading;
using kink.data;
namespace kink
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        private static App mInst;
        public static App Instance { get { return mInst; } }

        
#if DEV
        public DataUser curUser = new DataUser();
#else
        public DataUser curUser = null;
#endif

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            mInst = this;

            data.DataBase.StartConn();
        }

        public static void SyncInvoke(Action a)
        {
            KinkClient.Instance.Dispatcher.Invoke(a);
        }

#region api

#endregion
    }
}
