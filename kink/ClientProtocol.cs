﻿//using System;
//using System.Collections.Generic;
//using System.Collections;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.IO;

//using System.Net;
//using Newtonsoft.Json;

//namespace kink
//{

//    public class RetData
//    {
//        public string ReturnCode { get; private set; }
//        public string AdditionData { get; private set; }
//        public Hashtable AdditionTable { get; private set; }
//        public RetData(string data)
//        {
//            var splits = data.Split('#');
//            ReturnCode = splits[0];
//            if(splits.Length >1)
//            {
//                AdditionData = data.Substring(ReturnCode.Length+1);
//                Console.WriteLine("retdata:" + ReturnCode + "   " + AdditionData);
//                try
//                {
//                    AdditionTable = JsonConvert.DeserializeObject<Hashtable>(AdditionData);
//                }
//                catch (Exception e)
//                {
//                    Console.WriteLine("convert addition data error");
//                }
//            }
            
//        }
//    }

//    public static class ClientProtocol
//    {
//        public static string SERVER_URL = "http://127.0.0.1:5000/";

//        private static CookieCollection SAVED_COOKIES = null;

//        public static void Login(string user,string pwd)
//        {

//        }

//        public static string HttpGet(string query,Hashtable data)
//        {
//            string retString;
//            try
//            {
//                string reqstr = SERVER_URL + query;
//                if (data != null && data.Count != 0)
//                {
//                    reqstr += "?";
//                    foreach (DictionaryEntry p in data)
//                    {
//                        reqstr += string.Format("{0}={1}&", p.Key, p.Value);
//                    }
//                }
//                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(reqstr);
//                req.Method = "GET";
//                req.ContentType = "text/html;charset=UTF-8";

//                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
//                Stream respStream = response.GetResponseStream();
//                StreamReader streamReader = new StreamReader(respStream, Encoding.GetEncoding("utf-8"));

//                retString = streamReader.ReadToEnd();
//                streamReader.Close();
//                respStream.Close();
//            }
//            catch(Exception e)
//            {
//                return "-1";
//            }
            
//            return retString;
//        }

//        public static void ClearCookies()
//        {
//            SAVED_COOKIES = null;
//        }


//        public static void HttpGetAsync(string query,Hashtable data,Action<bool,string> callback)
//        {
//            try
//            {
//                string reqstr = SERVER_URL + query;
//                if (data != null && data.Count != 0)
//                {
//                    reqstr += "?";
//                    foreach (DictionaryEntry p in data)
//                    {
//                        reqstr += string.Format("{0}={1}&", p.Key, p.Value);
//                    }
//                }

//                HttpWebRequest request = HttpWebRequest.CreateHttp(reqstr);
//                request.Method = "GET";
//                request.ContentType = "text/html;charset=UTF-8";
//                request.CookieContainer = new CookieContainer();
//                if(SAVED_COOKIES != null)
//                    request.CookieContainer.Add(SAVED_COOKIES);

//                var ws = request.GetResponse() as HttpWebResponse;

//                SAVED_COOKIES = ws.Cookies;
//                Console.WriteLine("cookies:" + SAVED_COOKIES);

//                var stream = ws.GetResponseStream();

//                StreamReader streamReader = new StreamReader(stream, Encoding.GetEncoding("utf-8"));

//                var ret = streamReader.ReadToEnd();
//                streamReader.Close();

//                stream.Close();

//                Console.WriteLine("httpret:" + ret);

//                App.SyncInvoke(() => {
//                    callback(true,ret);
//                });

//            }
//            catch(Exception e)
//            {
//                App.SyncInvoke(() => {
//                    callback(false, e.Message);
//                });
//            }
//        }
//    }
//}
