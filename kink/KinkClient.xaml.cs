﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Arthas.Controls.Metro;
using Arthas.Utility.Media;

using kink.pages;

namespace kink
{
    

    /// <summary>
    /// KinkClient.xaml 的交互逻辑
    /// </summary>
    public partial class KinkClient : MetroWindow
    {
        public static KinkClient Instance { get; private set; }

        public static PageManager Pages;


        public PageRetail PageRetail { get { return this.FrameRetail.Content as PageRetail; } }
        public PageWash PageWash { get { return this.FrameWash.Content as PageWash; } }
        public PageIndex PageIndex { get { return this.FrameIndex.Content as PageIndex; } }
        public PageManagement PageManagement { get { return this.FrameManagement.Content as PageManagement; } }
        public PageInvoice PageInvoice { get { return this.FrameInvoice.Content as PageInvoice; } }
        public KinkClient()
        {
            InitializeComponent();
            Instance = this;
            

            this.Loaded += KinkClient_Loaded;
            MainMenu.SelectionChanged += MainMenu_SelectionChanged;


        }

        private void MainMenu_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (App.Instance.curUser == null)
            {
                MainMenu.SelectedIndex = 0;
            }
        }

        private void KinkClient_Loaded(object sender, RoutedEventArgs e)
        {
            Console.WriteLine(">>>>>");

            Pages = PageManager.Inst;
            Pages.OnCreate(Instance);

            RefreshPopMask();
        }


        #region utility

        private static bool mPopMsg = false;


        public void ShowMessageBoxInfo(string text, Action confirm, Action cancel = null, string btnconfirm = null, string btncancel = null)
        {
            ShowMessageBox(text, confirm, cancel, btnconfirm, btncancel);
        }


        private static void MsgBoxConfirm(object sender, RoutedEventArgs e)
        {
            Instance.PopupInfoBtnConf.Click -= MsgBoxConfirm;
            mPopMsg = false;
            if (actionConfirm != null) actionConfirm.Invoke();
            RefreshPopMask();
        }

        private static void MsgBoxCancel(object sender, RoutedEventArgs e)
        {
            Instance.PopupInfoBtnCanc.Click -= MsgBoxCancel;
            mPopMsg = false;
            if (actionCancel!=null) actionCancel.Invoke();
            RefreshPopMask();
        }

        private static Action actionConfirm;
        private static Action actionCancel;


        public static void ShowMessageBox(string text,Action confirm,Action cancel = null,string btnconfirm = null,string btncancel = null)
        {
            actionConfirm = confirm;
            actionCancel = cancel;

            mPopMsg = true;
            Instance.PopupInfoBtnCanc.Visibility = cancel == null ? Visibility.Collapsed : Visibility.Visible;
            Instance.PopupInfoBtnCanc.Content = string.IsNullOrEmpty(btncancel) ? "取消" : btncancel;
            Instance.PopupInfoBtnConf.Content = string.IsNullOrEmpty(btnconfirm) ? "确定" : btnconfirm;

            Instance.PopupInfoText.Text = text;
            Instance.PopupInfoText.Visibility = Visibility.Visible;
            Instance.PopupInfo.Visibility = Visibility.Visible;

            Instance.PopupProgress.Visibility = Visibility.Collapsed;

            Instance.PopupInfoBtnConf.Click += MsgBoxConfirm;
            if(cancel != null)
            {
                Instance.PopupInfoBtnCanc.Click += MsgBoxCancel;
            }

            RefreshPopMask();
        }

        private static void RefreshPopMask()
        {
            Console.WriteLine("refresh："+mPopMsg);
            if (mPopMsg)
                Instance.PopMask.Visibility = Visibility.Visible;
            else
                Instance.PopMask.Visibility = Visibility.Collapsed;   
        }

        #endregion

        
    }
}
