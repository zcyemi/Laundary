﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using kink.data;
using kink.pages;

namespace kink
{
    /// <summary>
    /// PageIndex.xaml 的交互逻辑
    /// </summary>
    public partial class PageIndex : Page,IPageBase
    {

        private KinkClient m_render;

        public PageIndex()
        {
            InitializeComponent();
        }

        public KinkClient render
        {
            get
            {
                return m_render;
            }

            set
            {
                m_render = value;
            }
        }

        public void OnCreate()
        {
            
            //signin
            IndexUserBtnSignin.Click += IndexUserBtnSignin_Click;
            IndexUserBtnLogout.Click += IndexUserBtnLogout_Click;
        }

        public void OnEnter()
        {
        }

        public void OnEvent(string name, object sender, RoutedEventArgs e)
        {
        }

        public void OnRefresh()
        {
            var user = App.Instance.curUser;
            IndexUserBtnLogout.Visibility = user == null ? Visibility.Collapsed : Visibility.Visible;
            IndexUserBtnSignin.Visibility = user == null ? Visibility.Visible : Visibility.Collapsed;

            if (user == null)
            {
                IndexUserInputName.IsEnabled = true;
                IndexUserInputPassword.IsEnabled = true;
                IndexUserInputName.Text = "";
                IndexUserInputPassword.Text = "";
            }
            else
            {
                IndexUserInputName.IsEnabled = false;
                IndexUserInputPassword.IsEnabled = false;
                IndexUserInputName.Text = user.Name;
                IndexUserInputPassword.Text = "";
            }
        }

        private void IndexUserBtnLogout_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            App.Instance.curUser = null;
            OnRefresh();
        }

        private void IndexUserBtnSignin_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            string name = IndexUserInputPassword.Text;
            string password = IndexUserInputPassword.Text;
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(password)) return;

            var q = DataBase.Context.Query<DataUser>();
            var u = q.Where(a => a.Name == name && a.Password == password).FirstOrDefault();
            if (u == null)
            {
                render.ShowMessageBoxInfo("Singin Failed", null);
            }
            else
            {
                App.Instance.curUser = u;
                OnRefresh();
                render.ShowMessageBoxInfo("Success", null);
            }
        }

       
    }
}
