﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using kink.data;
using kink.pages;
using Arthas.Controls.Metro;

namespace kink
{
    /// <summary>
    /// PageInvoice.xaml 的交互逻辑
    /// </summary>
    public partial class PageInvoice : Page,IPageBase
    {
        private KinkClient m_render;

        public KinkClient render
        {
            get
            {
                return m_render;
            }

            set
            {
                m_render = value;
            }
        }


        private int m_pageIndex = 1;


        public PageInvoice()
        {
            InitializeComponent();
        }

        public void OnCreate()
        {
            OnRefresh();

            InvoiceRefresh.Click += InvoiceRefresh_Click;
        }

        private void InvoiceRefresh_Click(object sender, RoutedEventArgs e)
        {
            OnRefresh();
        }

        public void OnEnter()
        {
        }

        public void OnEvent(string name, object sender, RoutedEventArgs e)
        {
        }

        public void OnRefresh()
        {
            List<DataOrder> data = null;

            var q = DataBase.Context.Query<DataOrder>();

            if (q.Count() == 0) return;
            data = q.OrderBy(a => a.OrderTime).TakePage(m_pageIndex, 20).ToList();


            foreach (var order in data)
            {
                order.Refresh();
            }

            InvoideOrderList.ItemsSource = data;
        }
    }
}
