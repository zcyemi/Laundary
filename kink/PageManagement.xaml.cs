﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


using kink.data;
using kink.pages;

namespace kink
{
    /// <summary>
    /// PageManagement.xaml 的交互逻辑
    /// </summary>
    public partial class PageManagement : Page,IPageBase
    {
        private KinkClient m_render;
        public PageManagement()
        {
            InitializeComponent();
        }

        public KinkClient render
        {
            get
            {
                return m_render;
            }

            set
            {
                m_render = value;
            }
        }

        public void OnCreate()
        {
            //user
            var q = DataBase.Context.Query<DataUser>();
            var users = q.ToList();
            MgrUserList.ItemsSource = users;
            MgrUserBtnDelete.Click += MgrUserBtnDelete_Click;
            MgrUserList.SelectionChanged += MgrUserList_SelectionChanged;

            //itemretail

            RefreshList();
            OnRefresh();

            MgrItemRetailBtnAdd.Click += MgrItemRetailBtnAdd_Click;
            MgrItemRetailBtnDel.Click += MgrItemRetailBtnDel_Click;
            MgrItemRetailBtnUpdate.Click += MgrItemRetailBtnUpdate_Click;
            MgrItemRetailList.SelectionChanged += MgrItemRetailList_SelectionChanged;
        }

        public void OnEnter()
        {
            throw new NotImplementedException();
        }

        public void OnEvent(string name, object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void OnRefresh()
        {
            ItemRetailOnRefresh();
        }

        #region user
        private void MgrUserBtnDelete_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (MgrUserList.SelectedItem != null)
            {
                var u = MgrUserList.SelectedItem as DataUser;
                render.ShowMessageBoxInfo("确定删除用户? " + u.Name, () =>
                {

                }, null, "删除");
            }
        }

        private void MgrUserList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var u = MgrUserList.SelectedItem as DataUser;
            MgrUserInputId.Text = u.Id.ToString();
            MgrUserInputUserName.Text = u.Name;
            MgrUserInputPWD.Text = u.Password;
        }
        #endregion


        #region itemretail

        private void MgrItemRetailList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ItemRetailOnRefresh();
        }

        public object selectItem { get { return MgrItemRetailList.SelectedItem; } }
        private void RefreshList()
        {
            var q = DataBase.Context.Query<DataRetailItem>();
            var items = q.ToList();
            MgrItemRetailList.ItemsSource = items;
        }

        private void MgrItemRetailBtnUpdate_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (selectItem == null)
            {
                var dname = MgrItemRetailTextName.Text;
                var dcate = MgrItemRetailTextCate.Text;
                var dprice = MgrItemRetailTextPrice.Text;

                if (string.IsNullOrEmpty(dname) || string.IsNullOrEmpty(dcate) || string.IsNullOrEmpty(dprice))
                {
                    return;
                }

                DataRetailItem item = new DataRetailItem();
                item.Name = dname;
                item.Category = dcate;
                try
                {
                    item.Price = float.Parse(dprice);
                }
                catch
                {
                    return;
                }

                var r = DataBase.Context.Insert<DataRetailItem>(item);

                render.ShowMessageBoxInfo("添加RetailItem：" + (r != null), () => {
                    if (r != null)
                    {
                        RefreshList();
                        OnRefresh();
                    }
                });
            }
            else
            {
                var d = selectItem as DataRetailItem;
                d.Name = MgrItemRetailTextName.Text;
                d.Category = MgrItemRetailTextCate.Text;
                d.Price = float.Parse(MgrItemRetailTextPrice.Text);

                render.ShowMessageBoxInfo("是否保存物品 Id:" + d.Id, () =>
                {
                    var result = DataBase.Context.Update<DataRetailItem>(d);

                    RefreshList();
                    OnRefresh();
                    render.ShowMessageBoxInfo(result == 0 ? "数据更新失败" : "保存成功", () => { });
                });

            }
        }

        private void MgrItemRetailBtnDel_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (selectItem == null)
            {
                return;
            }
            else
            {
                var d = selectItem as DataRetailItem;

                render.ShowMessageBoxInfo(string.Format("确认删除物品 Id:{0} {1} Price:{2}", d.Id, d.Name, d.Price), () =>
                {
                    var q = DataBase.Context.Delete<DataRetailItem>(d);
                    render.ShowMessageBoxInfo(q == 1 ? "删除成功" : "删除失败", () =>
                    {
                        RefreshList();
                        OnRefresh();
                    });
                }, null, "删除", "取消");

            }
        }

        private void MgrItemRetailBtnAdd_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (selectItem != null)
            {
                MgrItemRetailList.SelectedItem = null;
            }

            OnRefresh();
        }


        private void ItemRetailOnRefresh()
        {
            if (selectItem == null)
            {
                MgrItemRetailTextID.IsEnabled = false;
                MgrItemRetailTextID.Text = "(auto generate)";
                MgrItemRetailTextName.Text = "";
                MgrItemRetailTextCate.Text = "";
                MgrItemRetailTextPrice.Text = "";

                MgrItemRetailBtnUpdate.Content = "确认添加";
            }
            else
            {
                var d = selectItem as DataRetailItem;
                MgrItemRetailTextID.IsEnabled = false;
                MgrItemRetailTextID.Text = d.Id.ToString();
                MgrItemRetailTextName.Text = d.Name;
                MgrItemRetailTextCate.Text = d.Category;
                MgrItemRetailTextPrice.Text = d.Price.ToString();

                MgrItemRetailBtnUpdate.Content = "保存";
            }
        }

        #endregion
    }
}
