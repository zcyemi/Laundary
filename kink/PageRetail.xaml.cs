﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using kink.data;
using kink.pages;
using Arthas.Controls.Metro;
namespace kink
{
    /// <summary>
    /// PageRetail.xaml 的交互逻辑
    /// </summary>
    public partial class PageRetail : Page, IPageBase
    {
        private DataOrder m_currentOrder;

        private KinkClient m_render;

        public KinkClient render
        {
            get
            {
                return m_render;
            }

            set
            {
                m_render = value;
            }
        }

        public PageRetail()
        {
            InitializeComponent();
        }

        private void RetailItemListBtnRemove(object sender, RoutedEventArgs e)
        {
            OnEvent("RetailItemListBtnRemove", sender, e);
        }

        private void RetailItemListBtnAdd(object sender, RoutedEventArgs e)
        {
            OnEvent("RetailItemListBtnAdd", sender, e);
        }

        public void OnCreate()
        {
            m_currentOrder = new DataOrder();

            RetailItemList.SelectionChanged += RetailItemList_SelectionChanged;
            RetailOrderBtnDelete.Click += RetailOrderBtnDelete_Click;
            RetailOrderBtnClear.Click += RetailOrderBtnClear_Click;
            RetailOrderBtnSubmit.Click += RetailOrderBtnSubmit_Click;
            RefreshItemTypeList();
            OnRefresh();
        }

        private void RetailOrderBtnSubmit_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(RetailUserLName.Text) || String.IsNullOrEmpty(RetailUserFName.Text))
            {
                render.ShowMessageBoxInfo("CustInfo Missing",()=> { });
                return;
            }

            if (String.IsNullOrEmpty(RetailUserTel.Text))
            {
                render.ShowMessageBoxInfo("Tel is Null", () => { });
                return;
            }

            m_currentOrder.CustFName = RetailUserFName.Text;
            m_currentOrder.CustLName = RetailUserLName.Text;
            m_currentOrder.CustTel = RetailUserTel.Text;
            m_currentOrder.CustInfo = RetailUserInfo.Text;

            render.ShowMessageBoxInfo("Confirm to Submit this Order [" + m_currentOrder.GetTotal() + "]", () =>
            {

                m_currentOrder.OrderType = (int)DataOrderType.Retail;
                m_currentOrder.SetOrderTime(DateTime.Now);
                m_currentOrder.SetFetchTime(DateTime.Now);
                m_currentOrder.Generate();

                DataOrder r = null;
                try
                {
                    r = DataBase.Context.Insert<DataOrder>(m_currentOrder);
                }
                catch(Exception except)
                {
                    Console.WriteLine(except.Message + " " + except.StackTrace);
                    r = null;
                }

                if(r != null)
                {
                    Console.WriteLine("suc");
                    render.ShowMessageBoxInfo("添加订单成功!",()=> { });
                }
                else
                {
                    Console.WriteLine("fail");
                    render.ShowMessageBoxInfo("添加订单失败!", () => { });
                }

            }, () => {


            }, "Submit", "Cancel");

        }

        public void OnEnter()
        {
        }

        public void OnRefresh()
        {
        }

        public void OnEvent(string name, object sender, RoutedEventArgs e)
        {
            switch (name)
            {
                case "RetailItemListBtnRemove":
                    {
                        var btn = sender as MetroButton;
                        Console.WriteLine("remove " + btn.DataContext);
                        OrderRemoveItem(btn.DataContext as DataRetailItem);
                    }
                    break;
                case "RetailItemListBtnAdd":
                    {
                        var btn = sender as MetroButton;
                        Console.WriteLine("add " + btn.DataContext);
                        OrderAddItem(btn.DataContext as DataRetailItem);
                    }
                    break;
            }
        }

        private void RetailOrderBtnClear_Click(object sender, RoutedEventArgs e)
        {
            m_currentOrder.OrderItems.Clear();
            RefreshOrderItemList();
        }

        private void RetailOrderBtnDelete_Click(object sender, RoutedEventArgs e)
        {
            DataOrderItem item = render.PageRetail.RetailOrderList.SelectedItem as DataOrderItem;
            if (item != null)
            {
                m_currentOrder.OrderItems.Remove(item);
                RefreshOrderItemList();
            }
        }

        private void RetailItemList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
        }


        public void OrderAddItem(DataRetailItem item)
        {
            m_currentOrder.OrderAddItem(item);

            RefreshOrderItemList();
        }

        public void OrderRemoveItem(DataRetailItem item)
        {
            m_currentOrder.OrderRemoveItem(item);
            RefreshOrderItemList();
        }

        private void RefreshOrderItemList()
        {
            RetailOrderList.ItemsSource = m_currentOrder.OrderItems;
            var view = CollectionViewSource.GetDefaultView(RetailOrderList.ItemsSource);
            view.Refresh();

            RetailOrderTotal.Content = m_currentOrder.GetTotal();
        }

        private void RefreshItemTypeList()
        {
            var q = DataBase.Context.Query<DataRetailItem>();
            var items = q.ToList();
            RetailItemList.ItemsSource = items;
        }
    }
}
