﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Chloe;
using Chloe.SQLite;
using Chloe.Extensions;
using Chloe.Core;
using Chloe.Infrastructure;
using System.Data;
namespace kink.data
{
    public static class DataBase
    {
        private static SQLiteContext m_context;
        private static readonly string m_dbPath = AppDomain.CurrentDomain.BaseDirectory + "/Data/";

        public static SQLiteContext Context { get { return m_context; } }

        public static void StartConn(string dbname = "data.db3")
        {
            Console.WriteLine("start db conn");
            string conn = string.Format("Data Source={0}{1}", m_dbPath, dbname);   
            m_context = new SQLiteContext(new SQLiteConnectionFactory(conn));

        }

    }

    public class SQLiteConnectionFactory : IDbConnectionFactory
    {
        private string m_connstr;
        public SQLiteConnectionFactory(string connString)
        {
            m_connstr = connString;
        }

        public IDbConnection CreateConnection()
        {
            System.Data.SQLite.SQLiteConnection conn = new System.Data.SQLite.SQLiteConnection(m_connstr);
            Console.WriteLine(conn.State.ToString());
            return conn;

        }
    }

}
