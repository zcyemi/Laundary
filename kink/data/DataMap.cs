﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Chloe;
using Chloe.Infrastructure;
using Chloe.Mapper;
using Chloe.Entity;
namespace kink.data
{
    [Table("Users")]
    public class DataUser
    {
        [AutoIncrement]
        public int Id { get; set; }
        public int Role { get; set; }
        public string Name { get; set; }
        public DateTime? OpTime { get; set; }
        public string Password { get; set; }
    }

    [Table("WashItems")]
    public class DataWashItem
    {
        [AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageName { get; set; }
        public string Category { get; set; }
        public float Price { get; set; }
    }

    [Table("RetailItems")]
    public class DataRetailItem
    {
        [AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public float Price { get; set; }

        public DataRetailItem()
        {
            Price = 0;
        }
    }

    public enum OrderItemType
    {
        Retail = 0,
        Wash = 1,
        Dray = 2,
        Other = 3
    }

    public class DataOrderItem
    {
        public OrderItemType Itemtype { get; set; }
        public int ItemTypeId { get; set; }
        public string ItemName { get; set; }
        public int Count { get; set; }
        public float Price { get; set; }
        public float SubTotal { get; set; }

        public DataOrderItem(DataRetailItem retail)
        {
            Itemtype = OrderItemType.Retail;
            ItemTypeId = retail.Id;
            ItemName = retail.Name;
            Count = 0;
            Price = retail.Price;
            SubTotal = Count * Price;   
        }

        public void Refresh()
        {
            SubTotal = Price * Count;
        }
    }

    public enum DataOrderType
    {
        Retail = 0,
        Wash = 1,
        Dray = 2,
        Other = 3
    }

    [Table("Orders")]
    public class DataOrder
    {
        [AutoIncrement]
        public int OrderId { get; set; }
        public int OrderType = -1;
        [Chloe.Entity.NotMapped]
        public List<DataOrderItem> OrderItems = new List<DataOrderItem>();
        public string OrderItemStr { get; set; }
        public string CustFName { get; set; }
        public string CustLName { get; set; }
        public string CustTel { get; set; }
        public string CustInfo { get; set; }
        public long OrderTime { get; set; }
        public long FetchTime { get; set; }
        public long ModifyTime { get; set; }
        [NotMapped]
        public DateTime _OrderTime;
        [NotMapped]
        public DateTime _FetchTime;
        [NotMapped]
        public string CustNameDisplay { get { return CustLName + " " + CustFName; } }

        [NotMapped]
        public string OrderTimeDisplay { get { return _OrderTime.GetDateTimeFormats('g')[0].ToString(); } }

        [NotMapped]
        public static DateTime BASE_TIME;
        static DataOrder()
        {
            BASE_TIME = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
        }

        public void SetOrderTime(DateTime time)
        {
            _OrderTime = time;
            OrderTime =(long) (_OrderTime - BASE_TIME).TotalSeconds;
        }

        public void SetFetchTime(DateTime time)
        {
            _FetchTime = time;
            FetchTime = (long)(_FetchTime - BASE_TIME).TotalSeconds;
        }

        

        public float GetTotal()
        {
            float ret = 0;
            foreach(var i in OrderItems)
            {
                ret += i.SubTotal;
            }
            return ret;
        }

        public void Refresh()
        {
            _OrderTime = BASE_TIME.AddSeconds(OrderTime);
            _FetchTime = BASE_TIME.AddSeconds(FetchTime);
        }

        public void Generate()
        {
            if (OrderType == -1)
            {
                throw new Exception("OrderType not set");
            }
            OrderItemStr = Newtonsoft.Json.JsonConvert.SerializeObject(OrderItems);
        }


        public void OrderAddItem(DataRetailItem retail)
        {
            DataOrderItem matchItem = null;
            foreach(var item in OrderItems)
            {
                if (item.Itemtype != OrderItemType.Retail) continue;
                if(item.ItemTypeId == retail.Id)
                {
                    matchItem = item;
                    break;
                }
            }
            if(matchItem == null)
            {
                matchItem = new DataOrderItem(retail);
                OrderItems.Add(matchItem);
            }
            else
            {
                Console.WriteLine(matchItem.ItemTypeId + " " + matchItem.Count);
            }

            matchItem.Count++;
            matchItem.Refresh();
        }

        public void OrderRemoveItem(DataRetailItem retail)
        {
            DataOrderItem matchItem = null;
            foreach (var item in OrderItems)
            {
                if (item.Itemtype != OrderItemType.Retail) continue;
                if (item.ItemTypeId == retail.Id)
                {
                    matchItem = item;
                    break;
                }
            }
            if (matchItem == null) return;
            if(matchItem.Count == 1)
            {
                OrderItems.Remove(matchItem);
            }
            else
            {
                matchItem.Count -= 1;
                matchItem.Refresh();
            }
        }
    }


}
