﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Arthas.Controls.Metro;
using Arthas.Utility.Media;

using kink.pages;

using kink;

namespace kink.pages
{
    public class PageManager
    {
        private static PageManager m_inst;
        public static PageManager Inst
        {
            get {
                if (m_inst == null)
                {
                    m_inst = new PageManager();
                }
                return m_inst;
            }
        }

        //public PageManageUser PManageUser;
        //public PageIndex PIndex;
        //public PageManageItemRetail PMgrItemRetail;


        public PageRetail PRetail;
        public PageIndex PIndex;
        public PageManagement PManagement;
        public PageInvoice PInvoice;


        public PageManager()
        {

        }


        public void OnCreate(KinkClient render)
        {


            PRetail = render.PageRetail;
            PRetail.render = render;
            PRetail.OnCreate();

            PIndex = render.PageIndex;
            PIndex.render = render;
            PIndex.OnCreate();

            PManagement = render.PageManagement;
            PManagement.render = render;
            PManagement.OnCreate();

            PInvoice = render.PageInvoice;
            PInvoice.render = render;
            PInvoice.OnCreate();
        }
    }

    public interface IPageBase
    {
        KinkClient render { get; set; }
         void OnCreate();

        void OnEnter();

        void OnRefresh();

        void OnEvent(string name, object sender, RoutedEventArgs e);
    }

}
